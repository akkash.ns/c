#pragma once
#include "notepadClass.cpp"


int Notepad :: fileCount(string fileName){                     // function to count the number of versions
    struct dirent *d;      
    DIR *dr;
    int count =1;

    string path = "notepad_files/" + fileName + "_versions";  
    dr = opendir(path.c_str());
    if(dr!=NULL){
        for(d=readdir(dr); d!=NULL; d=readdir(dr)){ 
            string s = d->d_name;
            size_t found = s.find(".txt");   
            if (found != string::npos)
                count++;
        }
        closedir(dr);
        return count;
    }
    else{
        cout<<"\nError Occured";
        return 0;
    }
}
string Notepad :: versionPath(string path){           //fuction to return the path to write the new version
    string s = path;
    int pos = s.find(".");
    string fileName = s.substr(0,pos);
    int versionCount= fileCount(fileName);
            
    ostringstream streamObj;
    streamObj << versionCount;
    string ver = "/" + streamObj.str() + ".txt";

    path = "notepad_files/"+fileName+"_versions"+ver;
    return path;          
}