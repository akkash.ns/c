#include "notepadClass.h"
#include "fileNaming.cpp"

void Notepad :: editContent(){
    displayAllFiles();
    cout<<"\n\t Enter File name : ";
    cin>> m_fileName;
    m_fileName += ".txt";
    string path = "notepad_files/" + m_fileName;

    fstream textFile;
    textFile.open(path,ios::in);

    if(textFile){
        vector <string> fileContent;

        cout<<"\nContent of "<<m_fileName<<":-\n";
        fstream selectedFile;
        selectedFile.open(path, std::fstream::in);

        string text;
        cin.ignore (numeric_limits<streamsize>::max(), '\n');
        while (getline(selectedFile, text))
            fileContent.push_back(text);
        selectedFile.close();
        for(auto i: fileContent)
            cout<<i;

        cout<<"\n\n\t\t Edit content of "<< m_fileName;
        cout<<"\n\t\t\t MENU \n\t\t1. Find and Replace \n\t\t2. Append \n\t\t3. Insert \n\t\t4.EXIT";
        cout<<"\n Enter your choice : ";
        int choice;
        cin>>choice;
        while(choice<4){
            switch(choice){
                case 1 :  findAndReplace(m_fileName,fileContent);
                                  break;
                case 2 :  append(m_fileName,fileContent);
                                  break;
                case 3 :  insertAfter(m_fileName,fileContent);
                                  break;
                default : cout<<"\t Enter Valid Option";
                                   break; 

            }
            cout<<"\n\t\t\t MENU \n\t\t1. Find and Replace \n\t\t2. Append \n\t\t3. Insert \n\t\t 4. EXIT";
            cout<<"\n Enter your choice : ";
            cin >> choice;
        }
        return;
    }

    else{
        std::cout<<"\n Enter a valid file name ";
        return;
    }
}
void Notepad :: append(string outName,vector<string> lines){
    fstream fileOut,fileVer;
    string path = "notepad_files/" + outName ;
    fileOut.open(path, std::fstream::out);
    string newText;
    cout << "\n\t  Enter text to append : " ;
    cin.ignore (numeric_limits<streamsize>::max(), '\n');
    getline(cin,newText,'\t');

    path = versionPath(outName);

    fileVer.open(path, std::fstream::out);
    lines.push_back(newText);
    for(auto i : lines){
        fileOut << i ;
        fileVer << i ;
    }
    fileOut.close();
    fileVer.close();
    return;
}

void  Notepad :: findAndReplace(string outName,vector<string> lines){
    fstream fileOut,fileVer;
    string path = "notepad_files/" + outName ;
    fileOut.open(path,fstream::out);

    string search,replaceWith;
    ifstream fileIn;

    cout << "\n Enter word to search for: ";
    cin >>search;

    cout << "\n Enter word to replace: ";
    cin >>replaceWith;

    path = versionPath(outName);
    fileVer.open(path,fstream::out);

    for(auto s  : lines){
        if(s.find(search) != string::npos){
            fileOut<<replaceWith;
            fileVer<<replaceWith;
        }
        else{
            fileOut<<s; 
            fileVer<<s;
        }
    }
    fileOut.close();
    fileIn.close();
    return;
}

void Notepad :: insertAfter(string outName,vector<string> lines){
    fstream fileOut,fileVer;
    string path = "notepad_files/" + outName ;
    fileOut.open(path, std::fstream::out);

    string search,replaceWith;
    ifstream fileIn;

    cout << "\n Enter word to insert after : ";
    cin >>search;

    cout << "\n Enter word to replace: ";
    cin >>replaceWith;

    path = versionPath(outName);
    fileVer.open(path,fstream::out);

    for(auto s  : lines){
        if(s.find(search) != string::npos){
            fileOut<<replaceWith;
            fileVer<<replaceWith;
        }
        fileOut<<s; 
        fileVer<<s;
    }
    fileOut.close();
    fileIn.close();
    return;
}