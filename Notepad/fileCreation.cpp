#pragma once
#include "notepadClass.h"
#include "fileNaming.cpp"

void Notepad :: createFile(){
    if (mkdir("notepad_files", 0777) != -1)
                mkdir("notepad_files",0777);
                                                      // creates two files -> one main file(notepad_files/) -> one version file(notepad_files/filename_versions/1.txt
    cout<<"\n\t Enter File name : ";
    cin>> m_fileName;
    string tempName = m_fileName;              // temp string to store filename without .txt
    m_fileName += ".txt";
    string path = "notepad_files/"+m_fileName;         // path to create file inside notepad folder     

    fstream textFile,version;
    textFile.open(path,ios::in);

    if(textFile){
        cout << "\n\t File already exists";
        return;
    }

    else if(!textFile){
        m_version = "1";

        textFile.open(path,std::ios::out);
        tempName = "notepad_files/"+ tempName +"_versions";             //temp name contains the path to create sub folder with versions files

        if (mkdir(tempName.c_str(), 0777) == -1)
            cerr << "Error :  " << strerror(errno) << endl;

        path= tempName + "/"+ m_version + ".txt";           // path contains the path to place the version file
            version.open(path,ios::out);

        cout<<"\n Enter Text : ";
        string text;
        cin.ignore (numeric_limits<streamsize>::max(), '\n');
                
        getline(cin, text , '\t');                                // the content is written in bothe the main and 1.txt file
        textFile<<text;
        version<<text;
    }
    textFile.close();
    return;
}

void Notepad :: displayAllFiles(){
    struct dirent *d;       //dirent contains the directory stream. contains ino_t,d_ino ,file serialnumber,char d_name[],name of entry
    DIR *dr;
    dr = opendir("notepad_files");
    if(dr!=NULL){
    cout<<"List of all Text Files :";
    for(d=readdir(dr); d!=NULL; d=readdir(dr)){ 
        string s = d->d_name;
        size_t found = s.find(".txt"); //finds either .txt files or folders with _versions
        if (found != string::npos)
            cout <<"\n" <<"Main - " << s;
        size_t version = s.find("_versions");
        if(version != string::npos){
            struct dirent *ver;       //dirent contains the directory stream. contains ino_t,d_ino ,file serialnumber,char d_name[],name of entry
            DIR *dver;
            string s = d->d_name;
            cout << "\n" << s;
            int pos = s.find("_versions");
            string fileName = s.substr(0,pos);
            string path = "notepad_files/" + fileName + "_versions";
            dver = opendir(path.c_str());
            if(dver!=NULL){
                for(ver=readdir(dver); ver!=NULL; ver=readdir(dver)){ 
                    string sub = ver->d_name;
                    size_t found = sub.find(".txt");           //another dirent struct to show all the files in the folder filename_versions
                    if (found != string::npos)
                            cout <<"\n\t" << sub;
                }
            } 
            else{
                cout<<"\nError Occured";
                return;
            }  
        }                    
    }
    closedir(dr);
    }
}

void Notepad :: showContent(){
    cout<<"\n\t Enter File name : ";             // shows the content of the main file. Latest version
    cin>> m_fileName;
    m_fileName += ".txt";

    string path = "notepad_files/" + m_fileName;

    fstream textFile;
    textFile.open(path,std::ios::in);

    if(textFile){
        cout<<"\nContent of "<<m_fileName<<":-\n";
        string text;
        cin.ignore (numeric_limits<streamsize>::max(), '\n');
            while (getline(textFile, text)) 
                cout << "\n" <<text;
            textFile.close();
            return;
        }
    else{
        std::cout<<"\n Entered file does not exists";
        return;
    }
}
