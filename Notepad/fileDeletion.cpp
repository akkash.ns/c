
#include "notepadClass.h"
#include "fileNaming.cpp"

void  Notepad :: versioning(){
    cout<<"\n Enter file name to change versions : ";             // First finds the version folder based on the file name
    cin>>m_fileName;
    string tempName = m_fileName + ".txt";
    string path = "notepad_files/" + tempName ;
    fstream fileOut;
    fileOut.open(path,fstream::in);
    if(fileOut){
        cout<<"\n All available versions of " << m_fileName ;
        struct dirent *d;      
        DIR *dr;
        int count =1;
        path = "notepad_files/" + m_fileName + "_versions";          //Once folder is found provides the name of all version files
        dr = opendir(path.c_str());
        if(dr!=NULL){                                 
            for(d=readdir(dr); d!=NULL; d=readdir(dr)){ 
                string s = d->d_name;
                size_t found = s.find(".txt");   
                if (found != string::npos)
                    cout<<"\n" << s;
            }
            closedir(dr);
        }
        fileOut.close();
    }
    char choice;
    do{
        cout<<"\n Enter the version name to change to main version : ";                   // shows the content of the version file to chANGE
        cin >> tempName;
        tempName += ".txt";

        path = "notepad_files/"+m_fileName+"_versions/"+tempName;

        fstream versionFile;
        versionFile.open(path,fstream::in);
            
        vector <string> content;
        string text;
        cout<<"\n Content of version file -> "<< tempName;
        cin.ignore (numeric_limits<streamsize>::max(), '\n');
        while (getline(versionFile, text)){
            cout << "\n" << text;
            content.push_back(text);
        }
            
        versionFile.close(); 
        cout << "\n Do you want to change this file to main version [Y/N]: ";        //Copies the content to main file.
        cin >> choice;
        if(choice=='y' || choice=='Y'){
            string temp = m_fileName;
            temp+=".txt";
            path = "notepad_files/" + temp;
            fstream mainFile; 
            mainFile.open(path,fstream::out);

            for(auto i : content)
                mainFile << i;                   // this loop copies the version file to main file
            mainFile.close();
          
                    
            int count = fileCount(m_fileName)-1;

            fstream oldVersion;
            path = "notepad_files/"+m_fileName + "_versions/temp.txt";          // this loop copies the version file to temp
            oldVersion.open(path.c_str(),ios::out);
            for(auto i: content)
                oldVersion << i;   
            oldVersion.close();      

            ostringstream streamObj;
            streamObj << count;
            string ver = streamObj.str();

            path = "notepad_files/"+m_fileName + "_versions/"+ ver + ".txt";           // replace the current and old version
            string verpath = "notepad_files/"+m_fileName + "_versions/"+ tempName;
            rename(path.c_str(),verpath.c_str());
            remove(path.c_str());

            string temppath = "notepad_files/"+m_fileName + "_versions/temp.txt";     //places the temp file to updated version
            rename(temppath.c_str(),path.c_str());

            return;

        }
        else if(choice =='n' || choice =='N')
            break;
        else{
            cout << "\n Enter valid choice!!!";
            return;
        }
    }while(choice!='Y');
}

void Notepad :: deleteFile(){
    cout << "\n Enter the file name to delete : ";
    cin >> m_fileName;

    string tempName = m_fileName + ".txt";
    string path = "notepad_files/" + tempName ;
    fstream fileOut;
    fileOut.open(path,fstream::in);
    if(fileOut){
        cout<<"\n All available versions of " << m_fileName ;
        struct dirent *d;      
        DIR *dr;
        path = "notepad_files/" + m_fileName + "_versions";          //Once folder is found provides the name of all version files
        dr = opendir(path.c_str());
        if(dr!=NULL){                                 
            for(d=readdir(dr); d!=NULL; d=readdir(dr)){ 
                string s = d->d_name;
                size_t found = s.find(".txt");   
                if (found != string::npos)
                    cout<<"\n" << s;
                }
            closedir(dr);
        }
        fileOut.close();
    }
    if((fileCount(m_fileName)-1)==1){
        cout << "\n Only one version of the file exists!! Cannot delete ";
        return;
    }
    else{
        cout <<"\n Enter the version of the file to delete : ";          // removes the selected version file
        string deleteFile;
        cin >> deleteFile;
        deleteFile += ".txt";
        path = "notepad_files/" +  m_fileName + "_versions/" + deleteFile;
        if(remove(path.c_str()))
            cout << "\n" << deleteFile << " is deleted ";
        else if(!remove(path.c_str())){
            cout << "\n Error ocuured";
            return;
        }

        struct dirent *d;      
        DIR *dr;
        vector<string> versionFiles;
        path = "notepad_files/" + m_fileName + "_versions";              //file names are stored in vector to sort them in ascending
        dr = opendir(path.c_str());
        if(dr!=NULL){
            for(d=readdir(dr); d!=NULL; d=readdir(dr)){ 
                string s = d->d_name;
                size_t found = s.find(".txt"); 
                string sub = (s.substr(0,found));

                if (found != string::npos )
                    versionFiles.push_back(sub);
            }
        }
        closedir(dr);
        sort(versionFiles.begin(),versionFiles.end());
        int count = 1;

        for(auto i : versionFiles){                                      // count variable is used to keep count of no. of files,using count file rearranges and rename is done
            ostringstream streamObj;
            streamObj << count;
            string ver = streamObj.str();
            if(ver != i){
                string oldname = "notepad_files/" +  m_fileName + "_versions/" + i + ".txt";
                string newname = "notepad_files/" +  m_fileName + "_versions/" + ver + ".txt";
                rename(oldname.c_str(),newname.c_str());
            }
            count++;
        }  
        count--;
        ostringstream streamObj;
        streamObj << count;
        string main = streamObj.str();         // in case of recent file deletion the main file is updated with current recent.

        fstream versionFile,mainFile;
        vector <string> content;
        path = "notepad_files/" +  m_fileName + "_versions/" + main + ".txt";
        string text;
        versionFile.open(path,fstream::in);
        while (getline(versionFile, text))
            content.push_back(text);

        path = "notepad_files/" +  m_fileName + ".txt";
        mainFile.open(path,fstream::out);
        for(auto i: content)
            mainFile << i;
        mainFile.close();
        return;
    }
}