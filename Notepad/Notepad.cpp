
#include<iostream>
#include "fileCreation.cpp"
#include "fileDeletion.cpp"
#include "fileEdit.cpp"
using namespace std;
int main()
{
    cout<<"\f\t\t\t\t NOTEPAD ";
    cout<<"\n\t\t\t MENU \n\t\t1. Create text file \n\t\t2. View all exisiting file \n\t\t3. Show contents  \n\t\t4. Edit Contents \n\t\t5. Versioning\n\t\t6. Delete files \n\t\t7.EXIT";
    cout<<"\n Enter your choice : ";
    int choice;
    Notepad note;
    cin>>choice;
    while(choice<7)
    {
        switch(choice)
        {
            case 1 :    {
                        note.createFile();
                        break;
                    }
            case 2 :    {
                        note.displayAllFiles();
                        break;
                    }        
            case 3 :    {
                        note.showContent();
                        break;
                    }
            case 4 :    {
                        note.editContent();
                        break;
                    }
            case 5 :    {
                        note.versioning();
                        break;
                    }
            case 6 :    {
                        note.deleteFile();
                        break;
                    }
            default : cout<<"\t Enter Valid Option";
                        break; 

        }
        cout<<"\f\n\n\t\t\t MENU \n\t\t1. Create text file \n\t\t2. View all exisiting file \n\t\t3. Show contents  \n\t\t4. Edit Contents \n\t\t5. Versioning\n\t\t6. Delete files \n\t\t7.EXIT";
        cout<<"\n Enter your choice : ";
        cin>>choice;
    }
}