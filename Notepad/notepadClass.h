#include <iostream>
#include <dirent.h>
#include <fstream>
#include <dirent.h>
#include <cstdio>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <vector>
using namespace std;

#ifndef NOTEPADCLASS_H
#define NOTEPADCLASS_H

class Notepad
{
    private:
        string m_fileName;
        string m_version;

    public:
        void createFile(); 
        void displayAllFiles();
        void showContent();    
        void editContent();
        void append(string outName,vector<string> lines);
        void findAndReplace(string outName,vector<string> lines);
        void insertAfter(string outName,vector<string> lines);
        void versioning();
        void deleteFile();
        int fileCount(string fileName);
        string versionPath(string path);


};

#endif